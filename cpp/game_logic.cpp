#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "spawn", &game_logic::on_spawn },
      { "lapFinished", &game_logic::on_lap_finished },
      { "dnf", &game_logic::on_dnf },
      { "finish", &game_logic::on_finish },
      { "tournamentEnd", &game_logic::on_tournament_end }
    }, 
    tick(0),
    throttleoff(false)
{
  
  car.currentAngle = 0.0;
  car.previousAngle = 0.0;
  car.currentSpeed = 0.0;
  car.previousSpeed = 0.0;
  car.inPieceDistance = 0.0;
  car.previousDistance = 0.0;
  car.pieceIndex = 0;
  car.previousIndex = 0;
  car.currentThrottle = 0.0;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{

  ++tick;

  car.currentAngle = std::abs(data[0]["angle"].as_double());
  double deltaAngle = car.currentAngle - car.previousAngle;

  car.pieceIndex = data[0]["piecePosition"]["pieceIndex"].as_int();
  car.inPieceDistance = data[0]["piecePosition"]["inPieceDistance"].as_double();

  if( car.pieceIndex != car.previousIndex )
  {
    car.currentSpeed = car.inPieceDistance + ( pieces.at(car.previousIndex).length - car.previousDistance );
  }
  else
  {
    car.currentSpeed = car.inPieceDistance - car.previousDistance;
  }

  double estAcceleration = -0.0204081*car.currentSpeed + 0.204081*0.65;
  double currentAcceleration = car.currentSpeed - car.previousSpeed;

  std::cout << tick << ":"
            << " piece " << car.pieceIndex 
            << " angle " << car.currentAngle 
            << " position " << car.inPieceDistance 
            << " speed " << car.currentSpeed
            << " throttle " << pieces.at(car.pieceIndex).maxSpeed
            << " estimated acc " << estAcceleration
            << " actual acc " << currentAcceleration << std::endl;

  car.previousAngle = car.currentAngle;
  car.previousSpeed = car.currentSpeed;
  car.previousDistance = car.inPieceDistance;
  car.previousIndex = car.pieceIndex;

  return { make_throttle((pieces.at(car.pieceIndex).maxSpeed)/10) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;

  int index = car.previousIndex;
  ++pieces.at(index).crashes;
  for ( int i = 0; i < pieces.at(car.previousIndex).crashes + 2; ++i )
  {    
    std::cout << "Set " << index << " maxSpeed " << car.previousSpeed - 1.0 << " crashes" << pieces.at(index).crashes << std::endl;
    pieces.at(index).maxSpeed = car.previousSpeed - 1.0;
    if (index > 0)
    {
      --index;
    }
    else
    {
      index = pieces.size()-1;
    }
  }

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  std::cout << data["results"][0]["car"]["name"] << ": " 
            << data["results"][0]["result"]["ticks"] << ", "
            << data["results"][0]["result"]["millis"] << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  std::cout << "Your car: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  std::cout << "Game init: " << data.to_string() << std::endl;

  Piece tmp;

  for (int i = 0; i < data["race"]["track"]["pieces"].size(); ++i)
  {
    std::cout << "Piece " << i << ": ";

    tmp.length = 0.0;
    tmp.hasSwitch = false;
    tmp.radius = 0.0;
    tmp.angle = 0.0;
    tmp.maxSpeed = 7.0;
    tmp.crashes = 0;

    if ( !data["race"]["track"]["pieces"][i].get("length").is_null() )
    {
      tmp.length = data["race"]["track"]["pieces"][i].get("length").as_double();
      std::cout << tmp.length << std::endl;
      pieces.push_back(tmp);
    } 
    else 
    {
      tmp.radius = data["race"]["track"]["pieces"][i].get("radius").as_double();      
      tmp.angle = data["race"]["track"]["pieces"][i].get("angle").as_double();

      if ( tmp.angle > 0.0 )
      {
        tmp.radius = tmp.radius + 10.0;
      }
      else
      {
        tmp.radius = tmp.radius - 10.0;
      }

      std::cout << "Mutkapala " << tmp.radius << " " << tmp.angle << " " << 2.0*3.1415*tmp.radius*(std::abs(tmp.angle/360.0)) << std::endl;
      tmp.length = 2.0*3.1415*tmp.radius*(std::abs(tmp.angle/360.0));
      pieces.push_back(tmp);
    }
  }

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
  std::cout << "Spawn: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
  std::cout << "Lap Finished: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data)
{
  std::cout << "disqualified: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
  std::cout << "finished: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
  std::cout << "Tournament ends: " << data.to_string() << std::endl;
  return { make_ping() };
}

void game_logic::recursive(double& v, int t){
    --t;
    double estAcceleration = -0.0204081*car.currentSpeed + 0.204081*car.currentThrottle;
    v = v + estAcceleration;
    if (t > 0)
    {
      recursive(v,t);
    }
}